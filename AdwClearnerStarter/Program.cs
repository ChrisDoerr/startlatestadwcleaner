﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace AdwClearnerStarter
{
    class Program
    {
        static void Main(string[] args)
        {

            // Scan the current app directory (where the EXE is located)
            // and list files that match the AdwCleaner pattern.
            string rootPath     = AppDomain.CurrentDomain.BaseDirectory;
            string targetFiles  = @"" + rootPath;

            string[] allFiles   = Directory.GetFiles( targetFiles );

            if( allFiles.Length > 0 )
            {

                processFileList( allFiles );

            }

            Console.ReadKey();


        }

        public static void processFileList( string[] allFiles )
        {


            List<string> adwFiles   = new List<string>();
            string file             = "";
            
            // Hardcoded so you cannot start EVERY file you want!
            string pattern          = @"^adwcleaner_[0-9\.]+\.exe$";

            // Check for valid "AdwCleaner" EXE pattern
            for ( int i = 0; i < allFiles.Length; i++ )
            {

                file                = Path.GetFileName( allFiles[i] );

                bool match          = Regex.IsMatch( file, pattern );

                if ( match )
                {

                    adwFiles.Add( allFiles[i] );

                }

            }

            // Sort and then reverse the list
            adwFiles.Sort();
            adwFiles.Reverse();

            // The very first element SHOULD be the latest version!
            startLatestAdwCleanerAndCloseThisApp( adwFiles[0] );

        }

        public static void startLatestAdwCleanerAndCloseThisApp( string file )
        {

            // @todo another "if file exists"?

            // Start latest AdwCleaner EXE
            Process.Start( file );
            
            // Close THIS app
            Process.GetCurrentProcess().Kill();

        }

    }
}
